# Simple NodeMCU OLED clock  
  
This is a simple OLED clock using a NodeMCU.  
It shows the week day, day and month, as well as hours.  
It uses Wi-Fi and NTP Server.  

The main code was made based on 4 sources:  
1. https://www.arduinoecia.com.br/relogio-nodemcu-esp8266-display-oled-ntp/  
2.  https://lastminuteengineers.com/esp8266-ntp-server-date-time-tutorial/  
3.  https://github.com/arduino-libraries/NTPClient/issues/36#issuecomment-317214747  
4.  https://stackoverflow.com/a/225536  

From source #1 I used the OLED communication.  
From source #2 I used the NTP connection and the days matrix;  
From source #3 I used the getDate() and getMonth() functions (it was added directly on NTPClient library);  
From source #4 I used the 2 digits integer return function;
